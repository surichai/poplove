<?php

namespace App\Exports;

use App\Models\Customers;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomersExport implements  FromCollection, WithHeadings
{

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    private $headings = [
        'ชื่อ-นามสกุล',
        'เบอร์โทรศัพท์',
        'ที่อยู่',
        'รหัสไปรษณีย์',
        'หมายเหตุ',
        'น้ำหนัก (กรัม)',
        'กว้าง',
        'ยาว',
        'สูง',
        'มูลค่าสินค้า',
        'ยอดเก็บเงินปลายทาง'

    ];

    public function collection()
    {
        return collect($this->data);
    }
    public function headings(): array
    {
        return $this->headings;
    }


    /**
    * @return \Illuminate\Support\Collection
    // public function collection()
    // {
    //     $customers = Customers::all();

    //     $arr = [];
    //     foreach ($customers as $cus) {
    //         $arr['fullname'] = $cus->fullname;
    //         $arr['address'] = $cus->address;
    //     }

    //     return $customers;
    // }
     */
   
    // public function view():View
    // {
    //    foreach($data as $model){
    //     return view('exports.customers', [
    //         'header' => $this->headings(),
    //         'customers' => $model
    //     ]);
    //    }

    // }

   
  

}
