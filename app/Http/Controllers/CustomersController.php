<?php

namespace App\Http\Controllers;

use App\Exports\CustomersExport;
use App\Imports\CustomersImport;
use App\Models\Config;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator;
use DB;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use File;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class CustomersController extends Controller
{

    public function index()
    {
        $count = DB::table('customers')->count();
        $config =  DB::table('config')->first();

        return view('customers.app', compact('count', 'config'));
    }
    public function listItems()
    {
        return  [
            'age' => [
                "น้อยกว่า 13 ปี",
                "14 - 18 ปี",
                "19 - 23 ปี",
                "24 - 29 ปี",
                "30 ปีขึ้นไป",
            ],
            'sex' => [
                "ชาย",
                "หญิง",
                "ไม่ระบุ",
            ],
            'media' => [
                "Facebook",
                "Youtube",
                "Tiktok",
                "Link",
                "อื่นๆ",
            ]
        ];
    }

    public function register()
    {
        $items = $this->listItems();
        // dump($items['sex']);
        // exit();
        // return view('customers.register',compact('items'));
        return View::make('customers.register', ['items' => $items]);
    }
    public function store(Request $request)
    {

        $messages =  [

            'fullname.required' => 'กรุณากรอกข้อมูล ชื่อ - นามสกุล',
            'phone.required' => 'กรุณากรอกข้อมูล เบอร์โทร',
            'phone.unique' => 'เบอร์โทรนี้ มีอยู่ในระบบแล้ว!',
            'phone.digits_between' => 'เบอร์โทรไม่ถูกต้อง',
            'address.required' => 'กรุณากรอกข้อมูล ที่อยู่',
            'province.required' => 'กรุณาเลือกจังหวัด',
            'amphoe.required' => 'กรุณาเลือกเขต/อำเภอ',
            'tambon.required' => 'กรุณาเลือกแขวง/ตำบล',
            'age.required' => 'กรุณาเลือกอายุ',
            'sex.required' => 'กรุณาเลือกเพศ',
            'media.required' => 'กรุณาเลือกรู้จักแคมเปญ',


        ];
        $validator = Validator::make(
            $request->all(),
            [
                'fullname' => 'required',
                'phone' => 'required|unique:customers|numeric|digits_between:10,10',
                'address' => 'required',
                'province' => 'required',
                'amphoe' => 'required',
                'tambon' => 'required',
                'age' => 'required',
                'sex' => 'required',
                'media' => 'required',


            ],
            $messages
        );

        if ($request->media == 4 && empty($request->medianews_other)) {
            $message = [
                "medianews_other" => "กรุณากรอก รู้จักแคมเปญนี้จากสื่ออื่น"
            ];
            return redirect()->back()->withErrors($message)->withInput();
        }
        if ($validator->fails()) {
            $message = $validator->errors();
            return redirect()->back()->withErrors($message)->withInput();
        }



        $modelsInsert = [
            'fullname' => $request->fullname,
            'phone' => $request->phone,
            'address' => $request->address,
            'province' => $request->province,
            'amphoe' => $request->amphoe,
            'tambon' =>  $request->tambon,
            'zipcode' =>  $request->zipcode,
            'age' => $request->age,
            'sex' => $request->sex,
            'medianews' => $request->media,
            'medianews_other' => $request->medianews_other,

        ];


        DB::beginTransaction();
        try {
            //add data customers
            $customers = Customers::create($modelsInsert);

            DB::commit();
            return  Redirect::route('success', ['p' => $modelsInsert['fullname']]);
        } catch (\Exception $e) {

            DB::rollback();

            $message = [
                "msgError" => "เกิดข้อผิดพลาดระหว่างการลงทะเบียน โปรดลองใหม่"
            ];
            return redirect()->back()->withErrors($message)->withInput();
        }
    }


    public function success()
    {
        return view('customers.success');
    }


    public function checkTracking(Request $request)
    {

        $data = $request->all();
        if (isset($data) && $data['phone']) {
            $customers = DB::table('customers')->where('phone', $data['phone'])->first();
            if (isset($customers)) {
                return response()->json([
                    'status' => true,
                    'fullname' => $customers->fullname,
                    'phone' => $customers->phone,
                    'tracking' => $customers->tracking,
                ]);
            } else {
                return response()->json([
                    'status' => false,
                ]);
            }
        }
        // exit;
    }

    public function customers()
    {
        $config =  DB::table('config')->first();
        return view('customers.list',compact('config'));
    }

    public function getCustomersList(Request $request)
    {
        // $customers= DB::table('customers')->get();

        if ($request->ajax()) {
            $draw = $_POST['draw'];
            $row = $_POST['start'];
            $rowperpage = $_POST['length']; // Rows display per page
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
            $searchValue = $_POST['search']['value'];

            // Search 
            $searchQuery = " ";
            if ($searchValue != '') {
                $searchQuery = " 
                and ( 
     c.fullname LIKE '%" . $searchValue . "%' 
	OR c.phone LIKE '%" . $searchValue . "%' 
	OR c.address LIKE '%" . $searchValue . "%' 
	OR c.province LIKE '%" . $searchValue . "%' 
	OR c.amphoe LIKE '%" . $searchValue . "%' 
	OR c.tambon LIKE '%" . $searchValue . "%' 
	OR c.zipcode LIKE '%" . $searchValue . "%'
    )";
            }
            $customers = DB::select(DB::raw("SELECT * FROM customers c WHERE 1 " . $searchQuery . "
        
        order by " . $columnName . " " . $columnSortOrder . "  limit $row,$rowperpage
        "));

            // foreach($customers as& $m){
            //     $m['btnDel'] = '';
            //     $m['btnNote'] = '';
            //     dd($customers);
            // }
            // exit;

            $totalRecords =  DB::table('customers')->count();
            $response = array(
                "draw" => intval($draw),

                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecords,
                "aaData" => $customers
            );
            return json_encode($response);
        }
        //    return response()->json([
        //         'aaData'=>$customers,
        //         "iTotalRecords" => $totalRecords,
        //         "iTotalDisplayRecords" => $totalRecords,
        //     ]);
    }

    public function checkPhone(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if (isset($data) && $data['phone']) {
                $customers = DB::table('customers')->where('phone', $data['phone'])->first();
                if (isset($customers)) {
                    return response()->json([
                        'status' => true,
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                    ]);
                }
            }
        }
    }



    public function edit($id)
    {
        $items = $this->listItems();

        $customers = DB::table('customers')->where('id', $id)->first();
        return view('customers.edit', compact('items', 'customers'));
    }

    public function destroy(Customers $customers)
    {
        $customers->delete();

        return redirect()->route('customers.list')
            ->with('success', 'deleted successfully');
    }


    public function update(Request $request, Customers $customers)
    {
        $messages =  [

            'fullname.required' => 'กรุณากรอกข้อมูล ชื่อ - นามสกุล',
            'phone.required' => 'กรุณากรอกข้อมูล เบอร์โทร',
            // 'phone.unique' => 'เบอร์โทรนี้ มีอยู่ในระบบแล้ว!',
            'phone.digits_between' => 'เบอร์โทรไม่ถูกต้อง',
            'address.required' => 'กรุณากรอกข้อมูล ที่อยู่',
            'province.required' => 'กรุณาเลือกจังหวัด',
            'amphoe.required' => 'กรุณาเลือกเขต/อำเภอ',
            'tambon.required' => 'กรุณาเลือกแขวง/ตำบล',
            'age.required' => 'กรุณาเลือกอายุ',
            'sex.required' => 'กรุณาเลือกเพศ',
            'medianews.required' => 'กรุณาเลือกรู้จักแคมเปญ',


        ];
        $validator = Validator::make(
            $request->all(),
            [
                'fullname' => 'required',
                // 'phone' => 'required',
                'address' => 'required',
                'province' => 'required',
                'amphoe' => 'required',
                'tambon' => 'required',
                'age' => 'required',
                'sex' => 'required',
                'medianews' => 'required',


            ],
            $messages
        );

        if ($request->media == 4 && empty($request->medianews_other)) {
            $message = [
                "medianews_other" => "กรุณากรอก รู้จักแคมเปญนี้จากสื่ออื่น"
            ];
            return redirect()->back()->withErrors($message)->withInput();
        }
        if ($validator->fails()) {
            $message = $validator->errors();
            return redirect()->back()->withErrors($message)->withInput();
        }
        $modelsUpdate = [
            'fullname' => $request->fullname,
            'address' => $request->address,
            'province' => $request->province,
            'amphoe' => $request->amphoe,
            'tambon' =>  $request->tambon,
            'zipcode' =>  $request->zipcode,
            'age' => $request->age,
            'sex' => $request->sex,
            'medianews' => $request->medianews,
            'medianews_other' => $request->medianews == "4" ? $request->medianews_other : '',

        ];
        $cus =  DB::table('customers')->where('id',  $request->id)->update($modelsUpdate);
        return redirect()->route('customers.menagement')
            ->with('success', 'updated successfully');
    }

    public function customersDelete($id)
    {
        $cus =  DB::table('customers')->where('id',  $id)->delete();
        return redirect()->route('customers.menagement');
    }


    public function show()
    {
        return redirect()->route('customers.menagement');
    }

    public function export()
    {

        $paginate = 200;
        $cus =  Customers::where('phone', 'like', '0%')->paginate($paginate);
        $totalFile  = $cus->lastPage();
        $successGenFile = 0;
        for ($x = 1; $x <= $cus->lastPage(); $x++) {
            $customersModel =  $this->MapCustomers($paginate, $x);
            $successGenFile = $x;
            Excel::store(new CustomersExport($customersModel), time() . '_' . $x . '_customers.xlsx', 'exports');
        }
        if ($successGenFile  === $totalFile) {
            return redirect()->route('zip')->with('success', 'Export successfully');;
        }
    }
    public function MapCustomers($paginate, $page)
    {
        $d = Customers::where('phone', 'like', '0%')->whereNull('tracking')->paginate($paginate, ['*'], 'page', $page);
        $arr = [];
        foreach ($d as $k => $model) {
            $arr[$k]['fullname'] = $model->fullname;
            $arr[$k]['phone'] = $model->phone;
            $arr[$k]['address'] = $model->address . ' ' . $model->tambon . ' ' . $model->amphoe . ' ' . $model->province . ' ' . $model->zipcode;
            $arr[$k]['zipcode'] = $model->zipcode;
        }
        return $arr;
    }


    public function downloadZip()
    {
        $zip = new ZipArchive;

        $fileName = time() . '_POPLOVE_FILE.zip';
        $files = File::files(storage_path('app\public\poplove'));

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }

            $zip->close();
        }



        foreach ($files as $key => $v) {
            Storage::disk('exports')->delete(basename($v));
        }
        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        return response()->download(public_path($fileName))->deleteFileAfterSend(true);
    }

    public function import()
    {
        $file = request()->file('file');
        if ($file) {
            Excel::import(new CustomersImport, $file);
            return back()->with('success', 'Import File successfully');
        } else {
            return back()->with('error', 'Can not Import File');
        }
    }


    public function configUpdate(Request $request)
    {
    
        if ($request) {
            $models = [
                'limit_register' => $request->limit_register
            ];
            DB::table('config')->where('id',  $request->id)->update($models);
        }
        return back()->with('success', 'อัพเดท Limit จำนวนรับสมัคร สำเร็จ!');
    }
}
