<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Tambon;
use DB;

class TambonController extends Controller
{
    public function getProvinces()
    {
        $provinces = DB::table('tambons')
        ->select('province_code','province')
        ->groupBy('province_code', 'province')
        ->get();
        
        return $provinces;
    }
    public function getAmphoes($province)
    {   
        $amphoes = DB::table('tambons')
        ->select('amphoe_code','amphoe')
        ->where('province',$province)
        ->groupBy('amphoe_code', 'amphoe')
        ->get();
 
        return $amphoes;
    }
    public function getTambons($province,$amphoe)
    {
        $amphoes = DB::table('tambons')
        ->select('tambon_code','tambon')
        ->where('province',$province)
        ->where('amphoe',$amphoe)
        ->groupBy('tambon_code', 'tambon')
        ->get();

        // $tambons = Tambon::where('province',$province)
        //     ->where('amphoe',$amphoe)
        //     ->groupBy('tambon_code')
        //     ->get();
        return $amphoes;
    }
    public function getZipcodes($province,$amphoe,$tambon)
    {
        $zipcodes = Tambon::where('province',$province)
            ->where('amphoe',$amphoe)        
            ->where('tambon',$tambon)
            ->get();
        return $zipcodes;
    }
}