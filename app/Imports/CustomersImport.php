<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class CustomersImport implements ToCollection
{

    public function collection(Collection $rows)
    {
        echo '<pre>';
        foreach ($rows as$k=> $row) {
            if(isset($row[4]) && is_numeric($row[4])){
                $phone = $row[4];
                if(is_numeric($phone) && strlen($phone) ==10){
                    $tracking = $row[1];
                    $result = DB::table('customers')
                            ->where('phone', $phone)
                            ->update([
                                'tracking' => $tracking
                            ]);
                    }
                }
            }

    }
}
