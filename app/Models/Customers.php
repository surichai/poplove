<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{

    use HasFactory;

    /**
     * @var array
     */

    protected $table = 'customers';

    protected $fillable = [
        'fullname',
        'address', 'province',
         'amphoe', 'tambon', 'zipcode',
          'phone', 'age', 'sex', 'medianews', 'medianews_other', 'tracking', 'created_at', 'updated_at'
    ];
}
