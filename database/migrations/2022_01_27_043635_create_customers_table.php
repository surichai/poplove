<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('fullname');
            $table->string('address')->nullable();
            $table->string('province')->nullable();
            $table->string('amphoe')->nullable();
            $table->string('tambon')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('phone')->unique();
            $table->string('age')->nullable();
            $table->string('sex')->nullable();
            $table->string('medianews')->nullable();
            $table->string('medianews_other')->nullable();
            $table->string('tracking')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
