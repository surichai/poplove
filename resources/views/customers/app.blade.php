@extends('layouts.main')

@section('pageTitle')
POPLOVE SS3
@endsection
@section('style')

@endsection

@section('content')
@php 
$limit_register = 0;
$limit_register = (int)$config->limit_register;

@endphp
<div class="container home-container">
    <div class="home-form">
        <div class="mt-5">
            <div class="d-flex justify-content-center align-items-center">
                <img src="{{ url('/img/logo.png') }}" class="img-logo" width="290px">
            </div>
        </div>
        <div class="mt-3 text-center">
            <div class="project-title">POPLOVE SS3</div>
            <div class="project-subdetail mt-1">
                อยากบอกรักใคร ป๊อปทีนส่งของขวัญให้ !!
            </div>
        </div>
        @if($count <= $limit_register) 
        <div class="card-form">
            <button class="btn btn-danger btn-block btn-custom" id="registerBtn">
                <i class="fas fa-heart"></i>
                ส่งของขวัญ ให้กับคนที่คุณรัก <i class="fas fa-heart"></i></button>
        @endif
            <button class="btn btn-outline-traking btn-block btn-custom" id="trakingBtn">ตรวจสอบการรับรางวัล</button>

           
        </div>
        <div class="mt-4">
            <div class="d-flex justify-content-center align-items-center">
                <!-- <div class="sup-text " id="termsAndConditions">เงื่อนไขและข้อตกลงของกิจกรรม</div> -->
            </div>
        </div>
    </div>
    <div class="bubbling-heart">
        <div><i class="fa fa-heart fa-5x"></i></div>
        <div><i class="fa fa-heart fa-5x"></i></div>
        <div><i class="fa fa-heart fa-5x"></i></div>
        <div><i class="fa fa-heart fa-5x"></i></div>
      </div>
</div>





<div class="modal fade" id="checkTraking" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content" id="modal-warning-content">
            <div class="modal-body ">
                <div class="text-center login-title-box">
                    <h4 class="">ตรวจสอบเลข Tracking</h4>
                    <button type="button" class="close close-modal-btn" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-10" id="formResult">
                        <div class="text-center h5">
                            <p id="resultName"></p>
                            <p id="resultPhone"></p>
                            <div id="resultTracking" class=""></div>
                        </div>

                    </div>
                    <div class="col-md-10" id="formCheck">
                        <div class="text-center text-danger h5" id="resultFail"></div>
                        <div class="form-group">
                            <label for="usernameInput" class="font-weight-bold text-color-pink-1">เบอร์โทร</label>
                            <input type="tel" name="phone" id="phoneTracking" class="form-control text-lg"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                                style="font-size: 20px;" maxlength="10">
                        </div>
                        <div class=" mt-2">
                            <button type="button" id="btnCheck"
                                class="btn submit-form-btn btn-block text-dark shadow">ตรวจสอบ</button>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

</div>
</div>
@endsection


@section('scripts')
<script>
    $(document).ready(() => {
        var token = "{{ csrf_token() }}";

        $(".loadings").show();

        setTimeout(() => {
            $(".loadings").hide();
        }, 1000);

        function reset() {
            $('#formCheck').show();
            $('#formResult').hide();
            $('#phoneTracking').val('');
            $("#btnCheck").attr("disabled", true);
            $('#resultFail').hide();
        }

        $(document).on('click', '#trakingBtn', (event) => {
            $("#checkTraking").modal("show");
            reset();
        })

        $(document).on('click', '#termsAndConditions', (event) => {
            $("#termModal").modal("show");
        })

        $(document).on('click', '#registerBtn', function (event) {
            window.location.href = "{{ route('register')}}";
        })

        $("#btnCheck").attr("disabled", true);
        $('#formResult').hide();

        $("#phoneTracking").keyup(function () {
            let phoneTk = $("#phoneTracking").val();
            if (phoneTk && phoneTk.length == 10) {
                $("#btnCheck").attr("disabled", false);
            } else {
                $("#btnCheck").attr("disabled", true);
            }
        });

        $('.close-modal-btn').click(() => {
            reset()
        })



        $("#btnCheck").click(() => {
            $("#btnCheck").attr("disabled", true);

            $.ajax({
                headers: {
                    'x-csrf-token': token
                },
                method: 'POST',
                url: "{{ url('/') }}/check-traking",

                data: {
                    'phone': $("#phoneTracking").val()
                },
                success: function (response) {
                    if (response) {
                        if (response.status) {
                            $('#formCheck').hide();
                            $('#formResult').show();
                            $('#resultName').text('คุณ : ' + response.fullname);
                            $('#resultPhone').text('เบอร์โทร : ' + response.phone);
                            if (response.tracking) {
                                $('#resultTracking').text('เลขพัสดุ : ' + response.tracking)
                                $('#resultTracking').append( `
                                <br/>
                                <div class="mt-4" > 
                                <a 
                                target="_blank"
                                href="https://www.shippop.com/tracking?typeid=domestic&tracking_code=${response.tracking}">ติดตามพัสดุ </a>
                            </div>
                                ` );
                            } else {
                                $('#resultTracking').text('เลขพัสดุ : กำลังดำเนินการ')
                            }

                            $('#phoneTracking').val('');
                            $("#btnCheck").attr("disabled", true);
                            $('#resultFail').hide();

                        } else {
                            $('#formCheck').show();
                            $('#formResult').hide();
                            $('#resultFail').show();
                            $('#resultFail').text(
                                'ไม่พบข้อมูล กรุณาลองเบอร์โทรใหม่อีกครั้ง');
                            $("#btnCheck").attr("disabled", false);

                        }


                    }
                }
            })
        })

    })

</script>


@endsection
