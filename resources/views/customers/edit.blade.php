@extends('layouts.page') @section('pageTitle') ลงทะเบียนกิจกรรม @endsection
<div class="loadings"></div>


@section('content')

<div class="container">
        <form action="{{ route('customers.update', $customers->id) }}" method="POST">
            @csrf
            @method('PUT')
        <div class="d-flex justify-content-center mt-5">
            <img src="{{ url('/img/logo.png') }}" class="img-logo" width="290px" />
        </div>
        <div class="page-register mt-3">
            <div class="card card-register shadow">
                <div class="card-body p-5">
                    <h2 class="text-color-pink-1 register-h">ลงทะเบียนส่งของขวัญ ให้คนที่คุณรัก</h2>
                    <h4 class="text-color-pink-1">รายละเอียด</h4>
                    <div class="border border-bottom col-1"></div>
                    @error('msgError')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error : </strong>{{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @enderror
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="hidden"  name="id" value="{{ old('id',$customers->id) }}"
                                />
                                <label for="" class="font-weight-bold text-color-pink-1">ชื่อ - นามสกุล* (
                                    ใช้สำหรับการจัดส่ง
                                    )</label>
                                <input type="text" class="form-control" name="fullname" value="{{ old('fullname',$customers->fullname) }}"
                                    id="" placeholder="" />
                                @error('fullname')
                                <div class="text-danger mb-3">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="" class="font-weight-bold text-color-pink-1">เบอร์โทรศัพท์*
                                </label>
                                <input type="tel" class="form-control" name="phone" id="phoneInput" maxlength="10"
                                    value="{{ old('phone',$customers->phone) }}"
                                    readonly
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" />
                                @error('phone')
                                <div class="text-danger mb-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="" class="font-weight-bold text-color-pink-1">ที่อยู่</label>
                                <input type="text" class="form-control" name="address" maxlength="255"
                                    value="{{ old('address',$customers->address) }}" />

                                @error('address')
                                <div class="text-danger mb-3">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input_province" class="font-weight-bold text-color-pink-1">จังหวัด
                                            *</label>
                                        <select weidth="auto" title="กรุณาเลือกจังหวัด"
                                            class="form-control addressGroup selectpicker" data-live-search="true"
                                            data-dropup-auto="false" id="input_province" name="province">
                                            {{--
                                            <option value="">
                                                กรุณาเลือกจังหวัด
                                            </option>
                                            --}}
                                        </select>
                                        @error('province')
                                        <div class="text-danger mb-3">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input_amphoe" class="font-weight-bold text-color-pink-1">เขต/อำเภอ
                                            *</label>
                                        <select weidth="auto" title="กรุณาเลือกเขต/อำเภอ"
                                            class="form-control addressGroup selectpicker" data-live-search="true"
                                            data-dropup-auto="false" id="input_amphoe" name="amphoe">
                                            {{--
                                            <option value="">
                                                กรุณาเลือกเขต/อำเภอ
                                            </option>
                                            --}}
                                        </select>
                                        @error('amphoe')
                                        <div class="text-danger mb-3">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="input_tambon " class="font-weight-bold text-color-pink-1">แขวง/ตำบล
                                        *</label>
                                    <select weidth="auto" title="กรุณาเลือกแขวง/ตำบล"
                                        class="form-control addressGroup selectpicker" data-live-search="true"
                                        data-dropup-auto="false" id="input_tambon" name="tambon">
                                        {{--
                                        <option value="">
                                            กรุณาเลือกแขวง/ตำบล
                                        </option>
                                        --}}
                                    </select>
                                    @error('tambon')
                                    <div class="text-danger mb-3">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input_zipcode"
                                            class="font-weight-bold text-color-pink-1">รหัสไปรษณีย์</label>
                                        <input type="text" class="form-control addressGroup" name="zipcode"
                                            id="input_zipcode" maxlength="5"
                                            value="{{old('zipcode', $customers->zipcode ?? '')}}" readonly />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 border-left pl-5">
                            @if($items['age'])
                            <div class="form-group">
                                <label for="" class="font-weight-bold text-color-pink-1">อายุ *( ผู้ลงทะเบียน )
                                </label>
                                @foreach($items['age'] as $key => $age)
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="age" id="age-{{ $key }}" value="{{
                                        $key
                                    }}" {{ old('age',$customers->age ?? '') == $key ? 'checked' : '' }} />
                                    <label class="form-check-label redio-check" for="age-{{ $key }}">
                                        {{ $age }}</label>
                                </div>
                                @endforeach @error('age')
                                <div class="text-danger mb-3">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            @endif @if($items['sex'])
                            <div class="form-group">
                                <label for="" class="font-weight-bold text-color-pink-1">เพศ *
                                </label>
                                @foreach($items['sex'] as $key => $sex)
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sex" id="sex-{{ $key }}" value="{{
                                        $key
                                    }}" {{ old('sex',$customers->sex ?? '') == $key ? 'checked' : '' }} />
                                    <label class="form-check-label redio-check" for="sex-{{ $key }}">
                                        {{ $sex }}</label>
                                </div>
                                @endforeach @error('sex')
                                <div class="text-danger mb-3">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            @endif @if($items['media'])
                            <div class="form-group">
                                <label for="" class="font-weight-bold text-color-pink-1">รู้จักแคมเปญนี้จากสื่อใด *
                                </label>

                                @foreach($items['media'] as $key => $media)
                                <div class="form-check">
                                    <input class="form-check-input media-chenge" type="radio" name="medianews" id="media-{{
                                        $key
                                    }}" value="{{ $key }}"
                                        {{ old('medianews',$customers->medianews ?? '') == $key ? 'checked' : '' }} />
                                    <label class="form-check-label redio-check" for="media-{{ $key }}">
                                        {{ $media }}

                                        @if($key==4)
                                        <input type="text" name="medianews_other" id="medianews_other"
                                        maxlength="255"
                                            class="form-control" placeholder=""
                                            value="{{old('medianews_other', $customers->medianews_other ?? '')}}"
                                            aria-describedby="helpId" />
                                        @error('medianews_other')
                                        <div class="text-danger mb-3">
                                            {{ $message }}
                                        </div>
                                        @enderror

                                        @endif
                                    </label>
                                </div>
                                @endforeach @error('media')
                                <div class="text-danger mb-3">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-2 pb-4">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <button type="submit" class="btn submit-form-btn btn-danger btn-block shadow">
                        <i class="fas fa-save"></i>
                        บันทึกการแก้ไข
                    </button>
                </div>
                <!-- <div class="col-md-4 mt-3 mt-lg-0 mt-md-0">
                    <button type="button" class="btn shadow index-form-btn btn-block" id="backIndex">
                        <i class="fas fa-home"></i>
    
                        กลับหน้าหลัก
                    </button>
                </div> -->
            </div>
        </div>
    </form>
  
</div>

<div class="modal fade" id="termModal" tabindex="-1" aria-labelledby="termModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body p-4">
                <h4><u>ข้อกำหนดและเงื่อนไข การเข้าร่วมกิจกรรม</u></h4>
                <ol>
                    <li class="term">
                        ของรางวัลกิจกรรมมีจำนวน
                        <span class="text-color-pink">30,000 </span>
                        ชิ้นเท่านั้น
                    </li>
                    <li class="term">
                        ขอสงวนสิทธิ์
                        <span class="text-color-pink"> 1 </span> คนต่อ
                        <span class="text-color-pink">1</span> ชิ้นเท่านั้น
                    </li>
                    <li class="term">
                        กิจกรรมเริ่มวันที่
                        <span class="text-color-pink">4 ก.พ. 2565 - 10 ก.พ. 2565</span>
                        หรือจนกว่าของจะหมด
                    </li>
                    <li class="term">
                        ของรางวัลไม่สามารถแลกเปลี่ยนมูลค่าเป็นเงินสดได้
                        และไม่สามารถเปลี่ยนแปลงหรือโอนสิทธิ์ให้ผู้อื่นได้
                    </li>
                    <li class="term">
                        ทางบริษัทฯ จะไม่รับผิดชอบข้อผิดพลาด ปัญหาการใช้งาน
                        อันเกิดจากรางวัล หรือการขนส่งในกิจกรรมนี้ทุกกรณี
                    </li>
                    <li class="term">
                        ผู้ร่วมกิจกรรมจะอาศัยอยู่ในประเทศไทยเท่านั้น
                    </li>
                    <li class="term">
                        บริษัทฯ
                        ขอสงวนสิทธิ์ในการแก้ไขเปลี่ยนแปลงข้อกำหนดและเงื่อนไขและของรางวัลที่มีมูลค่าเท่ากัน
                        โดยไม่ต้องแจ้งให้ทราบล่วงหน้า
                    </li>
                    <li class="term">
                        บริษัทฯ มีสิทธิ์เปลี่ยนแปลง ปฏิเสธ ยกเลิก
                        หรือหยุดกิจกรรมได้โดยชอบธรรม
                        และไม่จำเป็นต้องแจ้งให้ทราบล่วงหน้า ทั้งนี้
                        ผู้เข้าร่วมกิจกรรม ไม่มีสิทธิ์ในการเรียกร้อง
                        หรือร้องขอผลจากการสูญเสียหรือความเสียหายที่เกิดขึ้นทั้งทางตรงและทางอ้อมจากบริษัทฯ
                    </li>
                    <li class="term">
                        พนักงานบริษัทเอส.ซี.เอส. สปอร์ตสแวร์
                        จำกัดและบริษัทในเครือ
                        ผู้ที่มีส่วนเกี่ยวข้องกับการจัดกิจกรรมในครั้งนี้
                        ไม่มีสิทธิ์เข้าร่วมกิจกรรมและรับของรางวัลใดๆ ทั้งสิ้น
                    </li>
                    <li class="term">
                        ผู้ที่ได้รับรางวัลยินยอมให้ทางบริษัทฯ
                        พิมพ์และ/หรือเผยแพร่รายชื่อ
                        และ/หรือรูปถ่ายของผู้ที่ได้รับรางวัลเพื่อการโฆษณาและประชาสัมพันธ์ธุรกิจ
                        ของบริษัทฯ ในปัจจุบันและ/หรือในอนาคตตามที่เห็นสมควร
                    </li>
                    <li class="term">
                        ผู้เข้าร่วมกิจกรรมได้อ่านและเข้าใจข้อความดีแล้ว
                        ตกลงยินยอมที่จะปฏิบัติตามข้อกำหนดและเงื่อนไขที่ระบุไว้ข้างต้นทุกประการ
                    </li>
                    <li class="term">
                        การตัดสินของคณะกรรมการถือเป็นที่สิ้นสุด
                    </li>
                </ol>
            </div>
            <div class="modal-footer d-block">
                <div class="my-2 text-center">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="confirmTerms" />
                        <label class="form-check-label font-weight-bold" for="confirmTerms">
                            ยอมรับข้อตกลง
                        </label>
                    </div>
                </div>
                <div class="d-flex justify-content-center mt-3">
                    <button type="button" id="submitForms" class="btn btn-term mr-3">
                        ตกลง
                    </button>
                    <button type="button" class="btn btn-outline-traking mr-2" data-dismiss="modal">
                        ยกเลิก
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @section('scripts')
<script>
    $(document).ready(() => {
        let token = "{{ csrf_token() }}";

        $(".loadings").show();

        setTimeout(() => {
            $(".loadings").hide();
        }, 1000);
        showProvinces();

        $(document).on("click", "#backIndex", function (event) {
            window.location.href = "{{ route('customers.index')}}";
        });

        $("#submitForms").attr("disabled", true);

        $(document).on("click", "#termsAndConditions", (event) => {
            $("#termModal").modal("show");
        });

        if ($('input[name=medianews]:checked').val() == 4) {
            $("#medianews_other").show();
        } else {
            $("#medianews_other").hide();
        }
        $(".media-chenge").on("change", function () {
            if ($('input[name=medianews]:checked').val() == 4) {
                $("#medianews_other").show();
            } else {
                $("#medianews_other").hide();
            }
        });

        $("#confirmTerms").change(function () {
            if ($("#confirmTerms").is(":checked")) {
                $("#submitForms").attr("disabled", false);
            } else {
                $("#submitForms").attr("disabled", true);
            }
        });

        $(document).on("click", "#submitForms", function () {
            $("#formRegister").submit(); // Submit the form
        });

        function clearSelect() {
            $("#input_amphoe").html("");
            $("#input_tambon").html("");
            $("#input_zipcode").val("");
        }

        function showProvinces() {
            if ("{{$customers->province ?? ''}}" == "") {
                $(".loadings").show();
            }
            //PARAMETERS
            let oldInput_province = "{{ old('province') }}";

            const response = [];
            const map = new Map();
            for (const item of dataTambons) {
                if (!map.has(item.province_code)) {
                    map.set(item.province_code, true); // set any value to Map
                    response.push({
                        province_code: item.province_code,
                        province: item.province,
                    });
                }
            }

            $(".loadings").hide();

            //UPDATE SELECT OPTION
            $("#input_province").html("");
            for (let item of response) {
                $("#input_province").append(
                    `<option ${
                        oldInput_province == item.province ? "selected" : ""
                    }  value="${item.province}" >
                    <div>${item.province}</div>
                </option>`
                );
            }
            $(".selectpicker.addressGroup").selectpicker("refresh");
            let selectData = "{{old('province', $customers->province ?? '')}}";
            $(".selectpicker#input_province").selectpicker("val", selectData);
            if (selectData != "") {
                showAmphoes();
            }
        }
        $("#input_province").on("change", showAmphoes);
        $("#input_amphoe").on("change", showTambons);
        $("#input_tambon").on("change", showZipcode);

        function showAmphoes() {
            clearSelect();
            var input_province = $("#input_province option:selected").val();
            let url = "{{ url('/') }}/province/" + input_province + "/amphoes";
            let oldInput_amphoe = "{{ old('amphoe',$customers->amphoe ?? '') }}";
            const response = [];
            const map = new Map();
            for (const item of dataTambons) {
                if (item.province == input_province) {
                    if (!map.has(item.amphoe)) {
                        map.set(item.amphoe, true);
                        response.push({
                            province_code: item.province_code,
                            province: item.province,
                            amphoe: item.amphoe,
                            tambon: item.tambon,
                            zipcode: item.zipcode,
                        });
                    }
                }
            }

            $(".loadings").hide();

            //UPDATE SELECT OPTION
            $("#input_amphoe").html("");
            for (let item of response) {
                $("#input_amphoe").append(
                    `<option value="${item.amphoe}" >
                <div>${item.amphoe}</div>
                </option>`
                );
            }
            $(".selectpicker.addressGroup").selectpicker("refresh");
            let selectData = "{{old('amphoe', $customers->amphoe ?? '')}}";
            $(".selectpicker#input_amphoe").selectpicker("val", selectData);
            if (selectData != "") {
                showTambons();
            }
        }

        function showTambons() {
            $("#input_tambon").html("");
            $("#input_zipcode").val("");
            var input_province = $("#input_province option:selected").val();
            var input_amphoe = $("#input_amphoe option:selected").val();

            let url =
                "{{ url('/') }}/province/" +
                input_province +
                "/amphoe/" +
                input_amphoe +
                "/tambons";

            let oldInput_tambon = "{{ old('tambon') }}";

            const response = [];
            const map = new Map();
            for (const item of dataTambons) {
                if (
                    item.province == input_province &&
                    item.amphoe == input_amphoe
                ) {
                    if (!map.has(item.tambon)) {
                        map.set(item.tambon, true);
                        response.push({
                            province_code: item.province_code,
                            province: item.province,
                            amphoe: item.amphoe,
                            tambon: item.tambon,
                            zipcode: item.zipcode,
                        });
                    }
                }
            }

            $(".loadings").hide();

            //UPDATE SELECT OPTION
            $("#input_tambon").html("");
            for (let item of response) {
                $("#input_tambon").append(
                    `<option value="${item.tambon}" >
                <div>${item.tambon}</div>
                </option>`
                );
            }
            //QUERY AMPHOES
            $(".selectpicker.addressGroup").selectpicker("refresh");
            let selectData = "{{old('tambon', $customers->tambon ?? '')}}";
            $(".selectpicker#input_tambon").selectpicker("val", selectData);
            if (selectData != "") {
                showZipcode();
            }
        }

        function showZipcode() {
            var input_province = $("#input_province option:selected").val();
            var input_amphoe = $("#input_amphoe option:selected").val();
            let input_tambon = $("#input_tambon option:selected").val();

            let url =
                "{{ url('/') }}/province/" +
                input_province +
                "/amphoe/" +
                input_amphoe +
                "/tambon/" +
                input_tambon +
                "/zipcodes";
            let oldInput_zipcode = "{{ old('zipcode') }}";

            const response = [];
            const map = new Map();
            for (const item of dataTambons) {
                if (
                    item.province == input_province &&
                    item.amphoe == input_amphoe &&
                    item.tambon == input_tambon
                ) {
                    if (!map.has(item.tambon)) {
                        map.set(item.tambon, true);
                        response.push({
                            province_code: item.province_code,
                            province: item.province,
                            amphoe: item.amphoe,
                            tambon: item.tambon,
                            zipcode: item.zipcode,
                        });
                    }
                }
            }

            $(".loadings").hide();

            //UPDATE SELECT OPTION
            $("#input_zipcode").val();

            for (let item of response) {
                input_zipcode.value = item.zipcode;
                if (oldInput_zipcode == item.zipcode) {
                    input_zipcode.value = oldInput_zipcode;
                }
                break;
            }
        }


        $("#phoneInput").change(() => {
            let phone = $('#phoneInput').val();
            if (phone && phone.length > 5) {
                $.ajax({
                    headers: {
                        'x-csrf-token': token
                    },
                    method: 'POST',
                    url: "{{ url('/') }}/check-phone",

                    data: {
                        'phone': phone
                    },
                    success: function (response) {
                        if(response.status) {
                            $('#termsAndConditions').attr('disabled','disabled');
                            Swal.fire(
                                {
                                    title: 'มีอยู่ในระบบแล้ว',
                                    type: "warning",
                                    confirmButtonColor: '#ff6c77',
                                    buttons: true,
                                    confirmButtonText: 'ปิดหน้า',

                                }

                            )
                        }else{
                            $('#termsAndConditions').removeAttr('disabled');
                        }
                    }
                })
            }
        })

    });

</script>

@endsection
