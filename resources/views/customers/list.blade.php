@extends('layouts.main') @section('pageTitle') จัดการข้อมูลผู้สมัคร @endsection
@section('style') @endsection @section('content')

<div class="card mt-5">
    <div class="card-body">
        <h3 class="card-title">จัดการข้อมูลผู้สมัคร</h3>
        @if ($message = Session::get('success'))
        <div class=" alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($notification = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $notification }}</strong>
        </div>
        @endif

        <div class="p-2">
            <div class="row">
                <div class="col-md-6">
                    <form action="{{ route('import') }}" method="POST" 
               class="mb-5"
                    enctype="multipart/form-data">
                        @csrf
                        <div class="form-group " >
                        <input type="file" name="file"  accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        <button class="btn btn-success">Import File Excel</button>
                       <a href="{{route('export')}}" class="btn btn-outline-primary"> <i class="fas fa-file-excel"></i> Export File Excel</a>

                        </div>
                    </form>

                    <label class="font-weight-bold">ตั้งค่า limit จำนวนรับสมัคร</label>

                    <form action="{{ route('config.update') }}" method="POST"   class="form-inline">
                        @csrf

                        <div class="form-group">
                            <input type="hidden" name="id"  class="form-control" value="{{old('id',$config->id)}}"> 

                            <input type="text" name="limit_register"   value="{{old('limit_register',$config->limit_register)}}" class="form-control"> 
                        </div>
                            <button type="submit" class="btn btn-primary pl-2">อัพเดทการตั้งค่า</button>
                    </form>


                </div>
            </div>

        </div>
        <table class="table datatable-menagement display nowrap" style="width: 100%">
            <thead>
                <tr>
                    <!-- <th>#</th> -->
                    <th>ชื่อ - นามสกุล</th>
                    <th>เบอร์โทรศัพท์</th>
                    <th>ที่อยู่</th>
                    <th>รหัสไปรษณีย์</th>
                    <th>อายุ</th>
                    <th>เพศ</th>
                    <th>แคมเปญนี้จากสื่อ</th>
                    <th>เวลาสมัคร</th>
                    <th>หมายเลขพัสดุ</th>
                    <th>หมายเหตุ</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

@endsection @section('scripts')
<script>
    $(document).ready(() => {
        $(".loadings").hide();

        let table;

        table = $(".datatable-menagement").DataTable({
            processing: false,
            serverSide: true,
            pageLength: 100,
            ajax: {
                url: "{{ route('customers.getCustomersList') }}",
                dataType: "json",
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}",
                },
            },
            columns: [{
                    data: "fullname",
                    name: "fullname",
                    visible: true,
                },
                {
                    data: "phone",
                    name: "phone",
                    visible: true,
                },
                {
                    data: "address",
                    name: "address",
                    visible: true,
                    render: function (data, type, full, meta) {
                        return `<strong>ที่อยู่ :</strong> ${full.address} <br>
                      <strong>แขวง/ตำบล :</strong> ${full.tambon} 
                      <strong>อำเภอ/เขต :</strong> ${full.amphoe} 
                      <strong>จังหวัด :</strong> ${full.province} 
                      <strong>รหัสไปรษณีย์ :</strong> ${full.zipcode} `;
                    },
                },
                {
                    data: "zipcode",
                    name: "zipcode",
                    visible: true,
                },
                {
                    data: "age",
                    name: "age",
                    visible: true,
                    render: function (data, type, full, meta) {
                        let itemsAge = [
                            "น้อยกว่า 13 ปี",
                            "14 - 18 ปี",
                            "19 - 23 ปี",
                            "24 - 29 ปี",
                            "30 ปีขึ้นไป",
                        ];
                        return itemsAge[full.age];
                    },
                },
                {
                    data: "sex",
                    name: "sex",
                    visible: true,
                    render: function (data, type, full, meta) {
                        let itemsSex = ["ชาย", "หญิง", "ไม่ระบุ"];
                        return itemsSex[full.sex];
                    },
                },

                {
                    data: "sex",
                    name: "sex",
                    visible: true,
                    className: "text-center",

                    render: function (data, type, full, meta) {
                        let itemsMedianews = [
                            "Facebook",
                            "Youtube",
                            "Tiktok",
                            "Link",
                            "อื่นๆ",
                        ];
                        return full.medianews == 4 ? `อื่นๆ  ( ${full.medianews_other} )` :
                            itemsMedianews[full.medianews];
                    },
                },

                {
                    visible: true,
                    className: "text-left",
                    data: "created_at",
                    name: "created_at",
                    render: function (data, type, full, meta) {
                        return full.created_at;
                    },
                },
                {
                    visible: true,
                    data: "tracking",
                    name: "tracking",
                },
                {
                    visible: true,
                    className: "text-left",
                    render: function (data, type, full, meta) {
                        return "";
                    },
                },
                {
                    visible: true,
                    className: "text-left ",
                    render: function (data, type, full, meta) {
                        let btnDel = "";
                        let btnEdit = "";
                        let btn = "";

                        let urlDel = `{{ url('/') }}/customers/${full.id}/edit/`;

                        let urlEdit = `{{ url('/') }}/customers/${full.id}/edit/`;

                        btnEdit =
                            `<a class="link text-info"  href="${urlEdit}"><i class="fas fa-pen"></i> </a>`;
                        btnDel =
                            `<a class="link text-danger" id="Del" href="#" data-id="${full.id}" ><i class="fas fa-trash"></i> </a>`;
                        btn = `<div class="text-center">
                         
                            <div class="p-1">${btnEdit} ${btnDel} </div>
                           
                        </div>`;

                        return btn;
                    },
                },
            ],
        });


        $('.datatable-menagement').on('click', '#Del', function (e) {
            e.preventDefault();
            console.log('ok');
            Swal.fire({
                    title: '<strong>ต้องการลบข้อมูลหรือไม่ ?</strong>',
                    icon: 'info',
                    type: 'error',
                    icon: 'error',

                    showCloseButton: true,
                    showCancelButton: true,
                }

            ).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    let id = $('#Del').attr("data-id")
                    let urlDel = `{{ url('/') }}/customers/delete/${id}`;
                    window.location.href = urlDel;
                } else if (result.isDenied) {}
            })

        });


    });

</script>

@endsection
