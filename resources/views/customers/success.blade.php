@extends('layouts.page') 
@section('pageTitle') ลงทะเบียนกิจกรรมสำเร็จ
@endsection
@section('content') 
@php $action =route('customers.store');
@endphp

<form
    name="formRegister"
    id="formRegister"
    method="post"
    action="{{ $action }}"
>
    @csrf
    <div class="container">
        <div class="d-flex justify-content-center mt-5">
            <img
                src="{{ url('/img/logo.png') }}"
                class="img-logo"
                width="290px"
            />
        </div>
        <div class="page-register mt-3">
            <div class="card card-register shadow">
                <div class="card-body p-5">
                    <div class="text-center  " >
                        <i class="fas fa-box fa-5x text-color-pink-1"></i>
                        <div class="font-weight-bold h1 text-color-pink-1 mt-1"  >ความรักของคุณกำลังเดินทาง... <i class="fas fa-heart " style=" animation: heartbeat 1s infinite;"></i></div>

                        @if(Request::get('p'))
                        <div class="font-weight-bold mt-3"  >
                            <h3>คุณ {{ Request::get('p') }} </h3>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="mt-2 pb-4">
                  <div class="row justify-content-center">
                      <div class="col-md-4 mt-3 mt-lg-0 mt-md-0">
                        <button
                        type="button"
                        class="btn shadow index-form-btn  btn-block"
                        id="backIndex"
                    >
                       <i class="fas fa-home"></i>  กลับหน้าหลัก
                    </button>
                      </div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
<script>
    $(document).ready(() => {
        let token = "{{ csrf_token() }}";
        $(document).on('click', '#backIndex', function(event) {
    window.location.href = "{{ route('customers.index')}}";
})

        $(".loadings").hide();

       

    })
</script>
@endsection
