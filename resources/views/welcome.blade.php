@extends('layouts.main')

@section('pageTitle')
Welcome
@endsection
@section('style')

@endsection

@section('content')

<div class="container">

    <div class="welcome-form">
    <div class="mt-4">
        <div class="d-flex justify-content-center align-items-center">
            <img src="{{ url('public/img/logo.png') }}" class="img-logo" width="100px">
        </div>
    </div>
    <div class="mt-3 text-center">
        <div class="project-title">เอ็นจีวี</div>
        <div class="project-detail">เพื่อลมหายใจเดียวกัน</div>
        <div class="project-subdetail mt-2">มาตรการบรรเทาผลกระทบผู้ขับขี่รถแท็กซี่สาธารณะ<br> ในเขตกรุงเทพฯ
            และปริมณฑล
        </div>
    </div>

    <div class="card-form">
        <button class="btn btn-primary btn-block btn-custom" id="registerBtn">ลงทะเบียน</button>

        <button class="btn btn-outline-primary btn-block btn-custom" id="loginBtn">ตรวจสอบ</button>
    </div>
</div>
</div>
@endsection
