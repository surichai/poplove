<?php

use App\Http\Controllers\CustomersController;
use App\Http\Controllers\TambonController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::redirect('/', 'index');

Route::get('index/', [CustomersController::class, 'index'])->name('app');
Route::get('register/', [CustomersController::class, 'register'])->name('register');
Route::get('success/', [CustomersController::class, 'success'])->name('success');
Route::get('list/menagement', [CustomersController::class, 'customers'])->name('customers.menagement');
Route::get('getCustomersList/', [CustomersController::class, 'getCustomersList'])->name('customers.getCustomersList');


Route::post('check-traking', [CustomersController::class, 'checkTracking'])->name('checkTracking');
Route::post('check-phone', [CustomersController::class, 'checkPhone'])->name('checkPhone');

Route::post('customers/getCustomersList', [CustomersController::class, 'getCustomersList'])->name('customers.getCustomersList');
Route::put('/update/{id}','CustomersController@update')->name('customers.update');
Route::get('/customers/delete/{id}', [CustomersController::class, 'customersDelete']);
Route::get('customers/export/', [CustomersController::class, 'export'])->name('export');
Route::post('customers/import/', [CustomersController::class, 'import'])->name('import');
Route::get('customers/zip/', [CustomersController::class, 'downloadZip'])->name('zip');

Route::post('/config/update',[CustomersController::class, 'configUpdate'])->name('config.update');

Route::resource('customers',CustomersController::class);

Route::get('/provinces', [TambonController::class, 'getProvinces'])->name('getProvinces');
Route::get('/province/{province_code}/amphoes', [TambonController::class, 'getAmphoes'])->name('getAmphoes');
Route::get('/province/{province_code}/amphoe/{amphoe_code}/tambons', [TambonController::class, 'getTambons'])->name('getTambons');
Route::get('/province/{province_code}/amphoe/{amphoe_code}/tambon/{tambon_code}/zipcodes', [TambonController::class, 'getZipcodes'])->name('getZipcodes');

